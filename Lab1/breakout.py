from sense_hat import SenseHat
import time
import random

XLIMIT = 7
YLIMIT = 7
random.seed()

class Block():
    
    def __init__(self, xpos=0, ypos=0, color=[0,0,255]):
        self.xpos = xpos
        self.ypos = ypos
        self.color = color     

class Blocks():
    
    def __init__(self):
        self.blocklist = []
        for xpos in range(0, XLIMIT + 1):
            self.blocklist.append( Block(xpos, 1, [0,0,255]) )
            self.blocklist.append( Block(xpos, 2, [0,0,255]) )
            self.blocklist.append( Block(xpos, 3, [0,0,255]) )
            
    def refresh(self):
        do_refresh = True
        for b in self.blocklist:
            if b.color != [0,0,0]:
                do_refresh = False
                break
        if do_refresh :
            for b in self.blocklist:
                b.color = [0,0,255]

    def draw(self, hat):
        for b in self.blocklist:
            hat.set_pixel(b.xpos, b.ypos, b.color)
    
    def block_exists(self, xpos, ypos):        
        for block in self.blocklist:
            if (block.xpos == xpos and
                block.ypos == ypos and
                block.color != [0,0,0]):
                # there is still an unhit block here
                return block
                    
    def checkBlockHit(self, ball):                
        # so the Ball pos is where it's going to be on next draw cycle
        block = self.block_exists(ball.xpos, ball.ypos)
        if block:
            hit = True
            block.color = [0,0,0]
            print ("HIT")
        
            # so that block is toast and will get blanked in next redraw
            # now, how to reflect the ball?
             
            A = Block() # A is the block to the L or R of target
            B = Block() # B is the block above or below the target
            
            B.xpos = ball.xpos
            
            if ball.ydir == -1 :
                B.ypos = ball.ypos + 1
            else:
                B.ypos = ball.ypos - 1
                    
            A.ypos = ball.ypos
            
            if ball.xdir == -1 :
                A.xpos = ball.xpos + 1
            else:
                A.xpos = ball.xpos - 1
            
            A_exist = self.block_exists(A.xpos, A.ypos)
            B_exist = self.block_exists(B.xpos, B.ypos)
            
            #print (A_exist, B_exist)
            
            if A_exist and not B_exist:
                ball.ydir = ball.ydir * -1
            elif B_exist and not A_exist:
                ball.xdir = ball.xdir * -1
            else:
                ball.xdir = ball.xdir * -1
                ball.ydir = ball.ydir * -1

        else:
            hit = False
        
class Ball():
    
    def __init__(self, xpos=0, ypos=2):
        self.xpos = xpos
        self.ypos = ypos
        self.xdir = 1
        self.ydir = 1
        self.color = [0,255,0]
        
        self.old_xpos = xpos
        self.old_ypos = ypos
    
    def draw(self, hat):
        hat.set_pixel(self.xpos, self.ypos, self.color)
        self.color = [0,255,0]
    
    def erase(self, hat):
        hat.set_pixel(self.old_xpos, self.old_ypos, [0,0,0])
        
    def move(self):
        self.old_xpos = self.xpos
        self.old_ypos = self.ypos
 
        xpos_next = self.xpos + self.xdir
        ypos_next = self.ypos + self.ydir
        
        wall_hit = False
        ceil_hit = False
        
        if xpos_next < 0 or xpos_next > XLIMIT : # wall hit
            self.xdir = self.xdir * -1
            wall_hit = True
        if ypos_next < 0 or ypos_next > YLIMIT : # ceiling/floor hit
            self.ydir = self.ydir * -1
            ceil_hit = True
            
        if (wall_hit and ceil_hit):
            self.xpos = self.xpos + self.xdir   # hit a corner - bump x only
        else:
            self.xpos = self.xpos + self.xdir
            self.ypos = self.ypos + self.ydir

class Paddle():

    def __init__(self, xpos = 1, width = 3):
        self.xpos = xpos
        self.old_xpos = xpos
        self.width = width
    
    def checkHit(self, ball):
        if ball.ypos == YLIMIT-1 and ball.ydir == 1:
            if self.xpos <= ball.xpos <= self.xpos + (self.width - 1):
                print("Paddle HIT")
                ball.ydir = -1
                if ball.xpos == self.xpos: # left edge
                    ball.xdir = -1
                elif ball.xpos == self.xpos + self.width - 1:
                    ball.xdir = 1
                else:
                    ball.xdir = 0            
        
    def move(self, direction):
        print('move padl', direction)
        self.old_xpos = self.xpos
        if direction == 'right':      # we are inverted here
            if self.xpos > -2:
                self.xpos -= 1
        if direction == 'left':
            if self.xpos + self.width - 1 < XLIMIT+2:
                self.xpos += 1
        
        
    def draw(self, hat):
        for n in range(self.width):
            if 0 <= self.old_xpos + n <= XLIMIT:
                hat.set_pixel(self.old_xpos +n, YLIMIT, [0,0,0])
        for n in range(self.width):
            if 0 <= self.xpos + n <= XLIMIT:
                hat.set_pixel(self.xpos +n, YLIMIT, [255,0,255])



if __name__ == "__main__":
    stime = time.time()
    
    hat = SenseHat()
    hat.set_rotation(180)
    hat.clear()
    
    ball = Ball()
    blocks = Blocks()
    padl = Paddle()
    
    gameloop = True
    
    while(gameloop):
        time.sleep(.1)
        padl.draw(hat)
        blocks.draw(hat)
        ball.erase(hat)
        ball.draw(hat)
        padl.checkHit(ball)   # has to be done before ball move
        ball.move()
        blocks.checkBlockHit(ball)
        blocks.refresh()
        
        for event in hat.stick.get_events():
            print("The joystick was {} {}".format(event.action, event.direction))
            if event.action == 'pressed' and event.direction == 'down':
                hat.clear()
                gameloop = False
            #if event.action == 'pressed' and event.direction == 'middle':
                
            if event.action == 'pressed':
                padl.move(event.direction)
            
        
        

    
    

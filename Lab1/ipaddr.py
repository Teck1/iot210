#!/usr/bin/python
# =============================================================================
#        File : ipaddr.py
# Description : Displays IP addr on the Raspberry Pi SenseHat 8x8 LED display
#               e.g. '10.193.72.242'
#      Author : Drew Gislsason
#        Date : 3/1/2017
# Modified by : Chris Higbie
# =============================================================================

# To use on bootup of :
# sudo nano /etc/rc.local add line: python /home/pi/ipaddr.py &
#

from sense_hat import SenseHat
import time
import socket
import fcntl
import struct
import os
import sensehat_smlib as smlib

# Given an ifname, uses ioctl to get its ipaddress
# (no need to connect a socket!)
def get_ip_address(ifname):
    try:
        SIOGIFADDR = 0x8915
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        addr = socket.inet_ntoa(fcntl.ioctl(s.fileno(),
                                            SIOGIFADDR,
                                            struct.pack(b'256s', (ifname[:15]).encode('utf-8'))
                                )[20:24])
        s.close()
    except:
        return 'error'
        
    return addr

def get_hostname():
    if socket.gethostname().find('.')>=0:
        hostname=socket.gethostname()
    else:
        hostname=socket.gethostbyaddr(socket.gethostname())[0]
    return hostname

# Given name of a network adapter check sysfs for up/down state
# Return True if 'up', False if not 'up'
def isup_sysfs_net(ifname):
    p = "/sys/class/net/" + ifname + "/operstate"
    if (os.path.exists(p)):
        f = open(p,'r')
        state = f.read()
        f.close()
        if (state.startswith('up')):
            return True
    return False

# Checks if ifname has autoconfigged using a 169. address for
#   8 seconds. If it gets a dhcp during this time, it exits.
def check_autoipaddr(ifname):
    sense.show_letter('A')
    t = 0
    while (get_ip_address(ifname).startswith('169')) :
        time.sleep(1)
        if t > 7 :
            return
        sense.set_pixel(t, 7, [0, 0, 255])
        t += 1

#Set initial display 
sense = SenseHat()
sense.set_rotation(180)
sense.show_letter('P')

# Check the wlan0 & eth0 adapters for UP state, continue if either are up,
# checks for a max of 8 seconds 
for t in range(8):
    if (isup_sysfs_net('wlan0') or isup_sysfs_net('eth0')):
        break
    sense.set_pixel(7, 7-t, [0, 0, 255])
    time.sleep(1)     

#display hostname
sense.show_message(get_hostname())

#display ip addresses, if adapters are up:  
if isup_sysfs_net('wlan0'):
    check_autoipaddr('wlan0')
    sense.show_message('wlan0')
    sense.show_message(get_ip_address('wlan0'))

if isup_sysfs_net('eth0'):
    check_autoipaddr('eth0')
    sense.show_message('eth0')
    sense.show_message(get_ip_address('eth0'))

sense.clear()

# Using smlib library, update temperature display

lnum = round(sense.get_temperature(), 1)
hist_temps = []
exit_loop = False

for tick in range(50):
    num = round(sense.get_temperature(), 1)

    dtext = str(num) + "C"

    if len(hist_temps) > 7:
        hist_temps.pop(0)
        
    if lnum < num:
        smlib.sm_scroll(dtext, smlib.r)
        hist_temps.append(smlib.r)
    elif lnum > num:
        smlib.sm_scroll(dtext, smlib.b)
        hist_temps.append(smlib.b)
    else:
        smlib.sm_scroll(dtext, smlib.g)
        hist_temps.append(smlib.g)

    lnum = num

    smlib.show_hist_bar(hist_temps, 7)

    for event in sense.stick.get_events():
        # print("The joystick was {} {}".format(event.action, event.direction))
        if event.action == 'pressed':
            exit_loop = True

    if exit_loop:
        break
        
smlib.sm_clear()




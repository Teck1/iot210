#!/usr/bin/python
# =============================================================================
#        File : sensehat_detect.py
# Description : detect presence of physical sensehat, and import either the
#               physical sensehat library, or the emulator
#
#       Usage : To use this file inside another python file, use the following:
#               'from sensehat_detect import SenseHat'
#
#        Date : 1/28/2018
# =============================================================================

# In order to install the sense_emu emulator:
#   sudo apt install python-sense-emu python3-sense-emu python-sense-emu-doc sense-emu-tools
#

import os

def physical_hat_attached():
    """
    Uses sysfs to determine if a physical Sense HAT is attached.
    Returns: True if physical Sense HAT attached, False otherwise
    """
    hat_path = "/proc/device-tree/hat/product"
    if (os.path.exists(hat_path)):
        f = open(hat_path,'r')
        product = f.read()
        f.close()
        if (product.startswith('Sense HAT')):
            return True
    else:
        return False
        
def import_sensehat():
    """
    Import SenseHat physical or emu, depending on if SenseHat is attached.
    Returns: None
    """
    global SenseHat
    if physical_hat_attached():
        from sense_hat import SenseHat
    else:
        from sense_emu import SenseHat

if __name__ == "__main__":
    
    if physical_hat_attached():
        print("Physical Sense HAT attached.")
    else:
        print("NO physical Sense HAT attached.")
    
    import_sensehat()

else:
    import_sensehat()
    

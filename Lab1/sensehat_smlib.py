# senseHAT custom library for small characters 3x5

from sense_hat import SenseHat
import time

n = [0, 0, 0]
r = [255, 0, 0]
g = [0, 255, 0]
b = [0, 0, 255]
w = [255, 255, 255]

num_0 = [ [0,0],[1,0],[2,0],[0,1],[2,1],[0,2],[2,2],[0,3],[2,3],[0,4],[1,4],[2,4] ]
num_1 = [ [2,0],[2,1],[2,2],[2,3],[2,4] ]  
num_2 = [ [0,0],[1,0],[2,0],[2,1],[0,2],[1,2],[2,2],[0,3],[0,4],[1,4],[2,4] ]
num_3 = [ [0,0],[1,0],[2,0],[2,1],[0,2],[1,2],[2,2],[2,3],[0,4],[1,4],[2,4] ]
num_4 = [ [0,0],[2,0],[0,1],[2,1],[0,2],[1,2],[2,2],[2,3],[2,4] ]
num_5 = [ [0,0],[1,0],[2,0],[0,1],[0,2],[1,2],[2,2],[2,3],[0,4],[1,4],[2,4] ]
num_6 = [ [0,0],[1,0],[2,0],[0,1],[0,2],[1,2],[2,2],[0,3],[2,3],[0,4],[1,4],[2,4] ]
num_7 = [ [0,0],[1,0],[2,0],[2,1],[2,2],[2,3],[2,4] ]  
num_8 = [ [0,0],[1,0],[2,0],[0,1],[2,1],[0,2],[1,2],[2,2],[0,3],[2,3],[0,4],[1,4],[2,4] ]
num_9 = [ [0,0],[1,0],[2,0],[0,1],[2,1],[0,2],[1,2],[2,2],[2,3],[2,4] ]
num_dot = [[1,4]]
num_minus = [ [0,2],[1,2],[2,2] ]
alp_F = [ [0,0],[1,0],[2,0],[0,1],[0,2],[1,2],[2,2],[0,3],[0,4] ]
alp_C = [ [0,0],[1,0],[2,0],[0,1],[0,2],[1,4],[2,4],[0,3],[0,4] ]

sn = {
    '0': num_0, 
    '1': num_1, 
    '2': num_2, 
    '3': num_3, 
    '4': num_4, 
    '5': num_5,
    '6': num_6,
    '7': num_7,
    '8': num_8,
    '9': num_9,
    '.': num_dot,
    '-': num_minus,
    'F': alp_F,
    'C': alp_C
    }

def sm_scroll(text, color):
    textlen = len(text)
    width = 4           # width in pixels of a single character

    for offset in range(7, -4 * textlen, -1):
        for ltr in range(textlen):
            for px in sn[text[ltr]]:
                if 0 <= px[0] + offset + width * ltr <= 7 :
                    hat.set_pixel(px[0] + offset + width * ltr, px[1], color)
    
        time.sleep(0.075)          
        
        for ltr in range(textlen):
            for px in sn[text[ltr]]:
                if 0 <= px[0] + offset + width * ltr <= 7 :
                    hat.set_pixel(px[0] + offset + width * ltr, px[1], n)

def sm_clear():
    hat.clear()

def sm_set_rotation(rot):
    hat.set_rotation(rot)

#given a list of up to 8 pixels, displays a line at given y location
def show_hist_bar(pxlist, y_loc):
    for ix in range(len(pxlist)):
            hat.set_pixel(ix, y_loc, pxlist[ix])

hat = SenseHat()
hat.set_rotation(180)

if __name__ == "__main__":
    
    hat.clear()
    
    stime = time.time()

    lnum = round(hat.get_temperature_from_pressure(), 1)
    temp_hist = []

    for tick in range(50):
        num = round(hat.get_temperature_from_pressure(), 1)

        dtext = str(num) + "C"

        if len(temp_hist) > 7:
            temp_hist.pop(0)
            
        if lnum < num:
            sm_scroll(dtext, r)
            temp_hist.append(r)
        elif lnum > num:
            sm_scroll(dtext, b)
            temp_hist.append(b)
        else:
            sm_scroll(dtext, g)
            temp_hist.append(g)

        lnum = num

        show_hist_bar(temp_hist, 7)
        
        print(time.time() - stime)

    hat.clear()











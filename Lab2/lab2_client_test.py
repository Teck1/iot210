#!/usr/bin/python

import httplib

HOST = "127.0.0.1"
PORT = 5000

def make_request(req):
    conn = httplib.HTTPConnection(HOST, PORT)
 
    verb = req[0]
    uri = req[1]
    
    if verb == 'POST':
        data = req[2]
        conn.request(verb, uri, data)
    else:
        conn.request(verb, uri)
    
    resp = conn.getresponse()
    data = resp.read()

    print("Status: " + str(resp.status))
    print("Reason: " + str(resp.reason))
    print("Data: " + str(data) + '\n')
    
    conn.close()
  
    
if __name__ == "__main__":
    test_requests = [ 
        ("GET", "/?data=10"),
        ("PUT", "/data?temp=10"),
        ("PUT", "/data?temp=20"),
        ("PUT", "/data?temp=30"),
        ("PUT", "/data?temp=40"),
        ("PUT", "/data?temp=50"),
        ("PUT", "/data?temp=60"),
        ("PUT", "/data?temp=70"),
        ("PUT", "/data?temp=80"),
        ("PUT", "/data?temp=80"),
        ("PUT", "/data?temp=70"),
        ("PUT", "/data?temp=60"),
        ("PUT", "/data?temp=50"),
        ("GET", "/data"),
        ("PUT", "/display?data"),
        ("PUT", "/display?letter=X"),
        ("GET", "/env?temperature"),
        ("GET", "/env?humidity"),
        ("GET", "/env?pressure"),
        ("DELETE", "/data"),
        ]
    for req in test_requests:
        make_request(req)

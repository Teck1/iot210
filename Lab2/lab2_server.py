#!/usr/bin/python
# =====================================================================
#        File : lab2_server.py
# Description : HTTP server accepting GET,PUT,POST & DELETE
#               uses data for PUT/POST
#               Can use Insomnia app to test api
# =====================================================================
from sense_hat import SenseHat
from flask import Flask, request

PORT = 5000

# global objects
app = Flask(__name__)
hat = SenseHat()
hat.set_rotation(180)
data_buf = []

def sense_display_data(data):
    hat.clear()
    
    if len(data) != 0:
        # map the data incoming into the 8x8 sense hat range
        data_min = min(data)
        data_max = max(data)
        span = data_max - data_min
        print("span: " + str(span))
        
        hat_x = 0
       
        
        for point in data:
            point_scaled = (point - data_min) / float(span)
            hat_y = int(7.0 * point_scaled)
            hat.set_pixel(hat_x, hat_y, [0, 255, 0])
            hat_x += 1

# APIs
@app.route("/", methods=['GET', 'PUT', 'POST', 'DELETE'])
def api_main():
    print("Main route")
    
    print("*** Method: " + request.method)
    
    for arg in request.args:
        print("*** Args:" + str(arg) + ":" + request.args[arg] + "")
        
    print("*** getdata:" + request.get_data())
    
    #200 OK
    #201 Created
    #204 No content
    return "OK", 200
    
@app.route("/env", methods = ['GET'])
def api_env():
    
    rsp_data = 'Environmental: '
    
    if 'humidity' in request.args:
        rsp_data += str(hat.get_humidity()) + " %rH\n"

    if 'pressure' in request.args:
        rsp_data += str(hat.get_pressure()) + " Millibars\n"
        
    if 'temperature' in request.args:
        rsp_data += str(hat.get_temperature()) + " C\n"
        
    return rsp_data

@app.route("/display", methods = ['PUT', 'POST'])
def api_display():
    
    if 'rotate' in request.args:
        r = int(request.args['rotate'])
        if r%90 == 0:
            hat.set_rotation(r, True)
        else:
            return 'Unsupported rotation value.', 415
    
    if 'letter' in request.args:
        print((request.args['letter'])[:1])
        hat.show_letter( (request.args['letter'])[:1] )
        
    if 'data' in request.args:
        sense_display_data(data_buf)
    
    return "OK", 200

@app.route("/data", methods = ['GET','PUT','POST', 'DELETE'])
def api_data():
    rsp_data = ""
    
    if request.method == 'PUT':
        for arg in request.args:
            if arg.startswith('temp'):
                if len(data_buf) > 7:   # keep the data buffer small
                    data_buf.pop(0)
                data_buf.append(int(request.args[arg]))
        return "Data accepted", 201
    
    if request.method == 'GET':
        if 'len' in request.args: 
            rsp_data += 'data len: ' + str(len(data_buf)) + "\n"
        
        for point in data_buf:
            rsp_data += str(point) + "\n"
        return rsp_data, 200
    
    if request.method == 'DELETE':
        del data_buf[:]     # python2
        # data_buf.clear()  # python3
        return "Data cleared", 200
    
# ============================== Main =================================

if __name__ == "__main__":

  print("Flask Server")

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
  

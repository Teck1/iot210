#!/usr/bin/python
# =====================================================================
#        File : sensehat_api_multi_protocol.py
# Description : HTTP server accepting GET,PUT,POST & DELETE
#               uses data for PUT/POST
#               Can use Insomnia app to test api
#               Data can be returned in JSON, XML or Protobufs
# =====================================================================
# from sense_hat import SenseHat
from sense_emu import SenseHat
from flask import Flask, request, Response
import json
import base64

import sensehat_data_pb2     #Google protobuf format file

# global objects ======================================================
PORT = 5000
app = Flask(__name__)
hat = SenseHat()
hat.set_rotation(180)


class env_data():
    def __init__(self, data_name, value, units):
        self.data_name = data_name
        self.value = value
        self.units = units
         

# APIs ================================================================

@app.route("/", methods=['GET'])
def api_main():
    print("Main route")
    return "OK", 200
    

@app.route("/api/v1/env" , methods = ['GET'])  
def api_env():
    
    rdata = []
    response = ''
    
    if 'Content-Type' in request.headers:
        content_type = request.headers['Content-Type']
    else:
        content_type = 'application/json'
    
    if 'humidity' in request.args:
        rdata.append(env_data('humidity', hat.get_humidity(), "%rH"))
    
    if 'pressure' in request.args:
        rdata.append(env_data('pressure', hat.get_pressure(), "Millibars"))

    if ('temperature' in request.args) or (len(request.args) == 0):
        rdata.append(env_data('temperature', hat.get_temperature(), "C"))
    
    if content_type == 'application/xml':
        response = '<group group="env">\n'
        for point in rdata:
            response += '<data_point>\n'
            response += '<data_name>' + point.data_name + '</data_name>\n'
            response += '<value>' + str(point.value) + '</value>\n'
            response += '<units>' + point.units + '</units>\n'
            response += '</data_point>\n'
        response += '</group>\n'
    
    elif content_type == 'application/json':
        for point in rdata:
            if len(response) > 0:       # comma separation if >1 data pt
                response += ','
            response += '{"data_name":"' + point.data_name + '",'
            response += '"value":"' + str(point.value) + '",'
            response += '"units":"' + point.units + '"}'
        
        response = '{ "group": { "data_point": [ ' + response
        response += '], "_group": "env"}' 
        response += '\n'
    
    elif content_type == 'application/protobuf':
        dlist = sensehat_data_pb2.DataList()
        dlist.Group = 'env'
        
        for point in rdata:
            dpoint = dlist.point.add()
            dpoint.data_name = point.data_name
            dpoint.value = point.value
            dpoint.units = point.units
        
        dlist_str = dlist.SerializeToString()
        
        response = base64.b64encode(dlist_str)
        
    else:
        return 'Protocol not implemented', 501
    
    # Create Flask Response object to be able to set Content-Type    
    resp_obj = Response(response)
    resp_obj.headers['Content-Type'] = content_type
    
    return resp_obj
    

# Main ================================================================ 

if __name__ == "__main__":

  print("Flask Server")

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)


"""
 curl 127.1:5000/api/v1/env
 curl 127.1:5000/api/v1/env -H 'Content-Type: application/xml'
 curl 127.1:5000/api/v1/env -H 'Content-Type: application/json'
 curl 127.1:5000/api/v1/env -H 'Content-Type: application/protobuf'
 curl 127.1:5000/api/v1/env?humidity -H 'Content-Type: application/xml'
 curl 127.1:5000/api/v1/env?pressure -H 'Content-Type: application/json'
 curl 127.1:5000/api/v1/env?temperature -H 'Content-Type: application/protobuf'
 curl 127.1:5000/api/v1/env?humidity\&temperature\&pressure -H 'Content-Type: application/xml'
 
"""

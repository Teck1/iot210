import base64
import sensehat_data_pb2

data = "CgNlbnYSFAoIaHVtaWRpdHkVnqP6QRoDJXJIEhUKC3RlbXBlcmF0dXJlFbnHDUIaAUM="

deco = base64.b64decode(data)

print(repr(deco))

mydata = sensehat_data_pb2.DataList()

mydata.ParseFromString(deco)

print(mydata)

"""
    mydata
    Group: "env"
    point {
      data_name: "humidity"
      value: 31.3298912048
      units: "%rH"
    }
    point {
      data_name: "temperature"
      value: 35.4450416565
      units: "C"
    }
"""

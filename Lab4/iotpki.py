#!/usr/bin/python
# =============================================================================
#        File : iotpki.py
# Description : Demonstrate public/private key
#      Author : Drew Gislsason
#        Date : 5/11/2017
# =============================================================================
# pip install pycrypto
# 
import sys
import os
from Crypto.PublicKey import RSA
from Crypto import Random

PRIVATE_KEY_FILENAME  = "privatekey.txt"
PUBLIC_KEY_FILENAME   = "publickey.txt"
DATA_FILENAME         = "data.txt"

def generate_key_files(publicname, privatename):

  random_generator = Random.new().read
  # key = RSA.generate(1024, random_generator)
  key = RSA.generate(2048, random_generator)

  # write private key file
  f = open(privatename,'w')
  f.write(key.exportKey())
  f.close()

  # write public key file
  f = open(publicname,'w')
  f.write(key.publickey().exportKey())
  f.close()

def read_private_key(privatename):
  f = open(privatename,'r')
  s = f.read()
  f.close()
  key = RSA.importKey(s)
  return key

if __name__ == "__main__":

  print "iotpki data to encrypt"

  if not os.path.exists(PUBLIC_KEY_FILENAME):
    generate_key_files(PUBLIC_KEY_FILENAME, PRIVATE_KEY_FILENAME)
    print "generated files: " + PUBLIC_KEY_FILENAME + ", " + PRIVATE_KEY_FILENAME

  key = read_private_key(PRIVATE_KEY_FILENAME)

  text = None
  for arg in sys.argv:
    if text == None: text = ''
    else: text = text + " " + arg

  # show the text
  print "\nPlain text, before encryption: " + text
  print "length of plain text (" + str(len(text)) + ")"

  # encrypt the text
  enc_text = key.publickey().encrypt(text, 32)
  print "\nEncrypted text, len (" + str(len(str(enc_text))) + ")"
  print '"' + str(enc_text) + '"'

  # decrypt the text
  plain_text = key.decrypt(enc_text)
  print "\nPlain text after decryption: " + text

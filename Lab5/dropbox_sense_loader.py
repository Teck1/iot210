#!/usr/bin/python
# --------------------------------------------------------------------
# dropbox_sense_loader.py
#  Sends data to a dropbox file. 
#  Monitors Sense Shield for Yaw changes. If a big enough change
#  occurs, data from sensors is stored. After at least 10 triggered
#  samples, encrypts and uploads the data to dropbox.
#
# Configuration file with Dropbox token and AES Symmetric key are 
#   stored in a file named dropbox_sense_config.py
# --------------------------------------------------------------------

import base64
import time
import pickle

from sense_hat import SenseHat
from Crypto.Cipher import AES
import dropbox


# Simple config loader that doesn't get added to git repository
# Contains global values for
#  TOKEN
#  SYMKEY
from dropbox_sense_config import TOKEN, SYMKEY


def dropbox_login(token):
    """
    Handles login to dropbox.
    """
    dbx = dropbox.Dropbox(token)
    try:
        info = dbx.users_get_current_account()
        return dbx
    except:
        print("Token invalid.")
        return None

def dbx_write_file(dbx, sensedata):
    """
    Writes the data to a file at dropbox.
    """
    filecontents = pickle.dumps(sensedata)
    filename = ("/" +
                "{:04}".format(time.localtime().tm_year) +
                "{:02}".format(time.localtime().tm_mon) +
                "{:02}".format(time.localtime().tm_mday) +
                "{:02}".format(time.localtime().tm_hour) +
                "{:02}".format(time.localtime().tm_min) +
                "{:02}".format(time.localtime().tm_sec) +
                "_sense_data.pkl")
    
    try:
        dbx.files_alpha_upload(filecontents, filename)
        print("File: {} written successfully.".format(filename))
        return len(filecontents)
    except:
        print("File upload to dropbox failed.")
        return None
    

def aes_encrypt(message, SYMKEY):
    """
    AES Symmetric key encryption
    """
    while len(SYMKEY) % 32:
        SYMKEY += SYMKEY
    encr = AES.new(SYMKEY[0:32], AES.MODE_ECB)
    
    while len(message) % 16:
        message += ' '
    
    return encr.encrypt(message)


def main():
    keep_running = True
    env = []
    last_yaw = 0
    
    while keep_running:
        
        yaw = sense.get_orientation().values()[2] 
               
        yaw_diff = abs(yaw - last_yaw)
        if yaw_diff > 180:
            yaw_diff = 360 - yaw_diff
        
        time.sleep(.1)  # Needs a time differential between yaw readings
        last_yaw = yaw
            
        if yaw_diff > 20:
            print("now {} last {} diff {}".format(yaw, last_yaw, yaw_diff))
            
            reading = []
            reading.append(sense.get_temperature())
            reading.append(sense.get_pressure())
            reading.append(sense.get_humidity())
            env.append(reading)
            print(reading)
            
            if len(env) % 10 == 0:
                print("Attempting data upload to dropbox")
                dbx_write_file(dbx, reading)
        
        # Press joystick in any direction to quit program
        for event in sense.stick.get_events():
            if event.action == 'pressed':
                keep_running = False
 
    sense.clear()


if __name__ == "__main__":
    
    # Set initial SenseHat display 
    sense = SenseHat()
    sense.set_rotation(180)
    sense.show_letter('D')

    # Initialize dropbox object
    dbx = dropbox_login(TOKEN)
    if dbx:
        print("Login to dropbox successful")
        print("Press the joystick in any direction to exit.")
        main()
    else:
        quit
        
    
